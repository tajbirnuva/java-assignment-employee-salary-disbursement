import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class Employee implements Jdbc {
    String employeePin;
    String name;
    String designation;
    String phoneNumber;
    Address address;
    Double accountBalance;

    public Employee(String employeePin, String name, String designation, String phoneNumber, Address address, Double accountBalance) {
        this.employeePin = employeePin;
        this.name = name;
        this.designation = designation;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.accountBalance = accountBalance;
    }

    public Employee() {
    }

    void saveEmployeeInDatabase(List<Employee> employeeList) throws SQLException {
        for (Employee employee : employeeList) {
            Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement();
            String query = "INSERT INTO employee (employeePin,name,designation,phoneNumber,accountBalance) VALUES ('" + employee.employeePin + "','" + employee.name + "','" + employee.designation + "','" + employee.phoneNumber + "'," + employee.accountBalance + ")";
            int i = statement.executeUpdate(query);
            if (i > 0) {
                System.out.println(employee.name + " - " + employee.employeePin + " Successfully Insert In Database.");
            }
        }
    }
}
