import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) throws SQLException {

        List<Employee> employeeList = Arrays.asList(
                new Employee("E-001", "Tajbir", "Manager", "01800000001",
                        new Address("Chittagong", "Feni", "Shanti Road"), 1000d),
                new Employee("E-002", "Tasuva", "Assistant Manager", "01800000002",
                        new Address("Chittagong", "Feni", "Shanti Road"), 1000d),
                new Employee("E-003", "Fahad", "Business Analyst", "01800000003",
                        new Address("Chittagong", "Feni", "Shanti Road"), 1000d),
                new Employee("E-004", "Parizad", "Hr", "01800000004",
                        new Address("Chittagong", "Feni", "Shanti Road"), 1000d),
                new Employee("E-005", "Faisal", "Manager", "01800000005",
                        new Address("Chittagong", "Feni", "Shanti Road"), 1000d),
                new Employee("E-006", "Maruf", "Business Analyst", "01800000006",
                        new Address("Chittagong", "Feni", "Shanti Road"), 1000d),
                new Employee("E-007", "Eyana", "Hr", "01800000007",
                        new Address("Dhaka", "Dhaka", "Shanti Nagar"), 1000d),
                new Employee("E-008", "Adiba", "Hr", "01800000008",
                        new Address("Dhaka", "Dhaka", "Shanti Nagar"), 1000d),
                new Employee("E-009", "Tawsif", "Assistant Manager", "01800000009",
                        new Address("Dhaka", "Dhaka", "Shanti Nagar"), 1000d),
                new Employee("E-010", "Farhan", "Manager", "01800000010",
                        new Address("Dhaka", "Dhaka", "Shanti Nagar"), 1000d)
        );

        Employee e = new Employee();
        e.saveEmployeeInDatabase(employeeList);

        CompanyAccount companyAccount = new CompanyAccount();
        companyAccount.depositCompanyAccountBalance();
        companyAccount.disburseSalary(employeeList);
        companyAccount.companyAccountDepositDetails();
    }
}