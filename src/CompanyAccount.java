import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CompanyAccount implements Jdbc {
    Double currentBalance = 0d;
    Double baseSalary = 0d;
    Double hourlyRate = 10d;
    Double extraRate = 15d;
    List<Double> depositAmountList = new ArrayList<>();

    Scanner sc = new Scanner(System.in);

    Double calculateSalary(String designation) {
        if (designation.equals("Manager")) {
            baseSalary = 5000d;
        } else if (designation.equals("Assistant Manager")) {
            baseSalary = 4000d;
        } else if (designation.equals("Business Analyst")) {
            baseSalary = 3000d;
        } else if (designation.equals("Hr")) {
            baseSalary = 2000d;
        }
        return baseSalary + (hourlyRate * extraRate);
    }

    void depositCompanyAccountBalance() {
        System.out.print("Enter Company Bank Account Balance : ");
        Double rechargeAmount = sc.nextDouble();
        if (rechargeAmount == 0) {
            System.out.println("Bank Account Balance Can’t be Zero\n");
            depositCompanyAccountBalance();
        } else if (rechargeAmount < 0) {
            System.out.println("Bank Account Balance Can’t be Negative\n");
            depositCompanyAccountBalance();
        } else {
            currentBalance += rechargeAmount;
            depositAmountList.add(rechargeAmount);
        }
    }

    void disburseSalary(List<Employee> employeeList) throws SQLException {
        for (Employee employee : employeeList) {
            Double salary = calculateSalary(employee.designation);
            while (currentBalance < salary) {
                System.out.println("\nInsufficient Company Account Balance. Please Deposit Money Into The Account\n");
                depositCompanyAccountBalance();
            }
            currentBalance -= salary;
            employee.accountBalance += salary;
            updateEmployeeAccountBalanceInDatabase(employee);
        }
    }

    void updateEmployeeAccountBalanceInDatabase(Employee employee) throws SQLException {
        Connection connection = DriverManager.getConnection(url, username, password);
        Statement statement = connection.createStatement();
        String query = "UPDATE employee SET accountBalance = " + employee.accountBalance + " WHERE employeePin = '" + employee.employeePin + "'";
        int i = statement.executeUpdate(query);
        if (i > 0) {
            System.out.println(employee.name + " - " + employee.employeePin + " Salary Paid and Successfully update In Database.");
        }
    }

    void companyAccountDepositDetails() {
        System.out.println("\nThe Money Has Been Deposited In The Company Account " + depositAmountList.size() + " Times");
        if (depositAmountList.size() > 1) {
            System.out.println("First Deposit Amount : " + depositAmountList.get(0));
            System.out.println("Last Deposit Amount : " + depositAmountList.get(depositAmountList.size() - 1));
        }
    }
}
