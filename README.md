# Java Assignment - Employee Salary Disbursement

Create a program that calculates the salary of employees and stores their details in a relational database. First of all you have to create Employee Class having the following members:
- Employee Pin
- Name
- Designation (Manager, Assistant Manager, Business Analyst, Hr)
- Phone number
- Address (Must contain Division, District, Area/Road)
- Account Balance
In the initial phase, you must store 10 employee details in the database. 

In the next step, create a CompanyAccount Class and define four variables named: currentBalance, baseSalary, hourlyRate, and extraRate. Using a method known as 'disburseSalary', the salary of the employee is calculated based on their base salary, and the calculated salary will be paid from the company's bank account. The base salary of Manager, Assistant Manager, Business Analyst and Hr is 5000, 4000, 3000 and 2000 respectively. If the bank balance becomes less than the amount paid, then a deposit should be done during the salary disbursement and remember the deposit amount cannot be negative or zero.

Whenever the company pays the salary to the employee, the paid salary will be credited to the employee's account and stored in the database.

Finally, you should show how many times money has been deposited into the company's account. It’s also required that if more than one amount has been deposited into a company's account, both the first and last amount must be shown.

**_Hints:_**

```sh
No need to insert the employee's address in the database
Total Salary = baseSalary + (hourlyRate * extraRate)
To avoid repetitive code you have to follow OOP approach 
Create Proper Additional Class or Interface  for Jdbc and other Requirements 
```

## Output
```html
Employee Name 1 - Employee  Pin 1 Successfully Insert In Database.
Employee Name 2 - Employee  Pin 2 Successfully Insert In Database.
Employee Name 3 - Employee  Pin 3 Successfully Insert In Database.
Employee Name 4 - Employee  Pin 4 Successfully Insert In Database.
Employee Name 5 - Employee  Pin 5 Successfully Insert In Database.
Employee Name 6 - Employee  Pin 6 Successfully Insert In Database.
Employee Name 7 - Employee  Pin 7 Successfully Insert In Database.
Employee Name 8 - Employee  Pin 8 Successfully Insert In Database.
Employee Name 9 - Employee  Pin 9 Successfully Insert In Database.
Employee Name 10 - Employee  Pin 10 Successfully Insert In Database.

Enter Company Bank Account Balance : - 10000
Bank Account Balance Can’t be Negative

Enter Company Bank Account Balance: 10000

Employee Name 1 - Employee  Pin 1 Salary Paid and Successfully update In Database.
Employee Name 2 - Employee  Pin 2 Salary Paid and Successfully update In Database.
Employee Name 3 - Employee  Pin 3 Salary Paid and Successfully update In Database.

Insufficient Company Account Balance. Please Deposit Money Into The Account

Enter Company Bank Account Balance : 0
Bank Account Balance Can’t be Zero 

Enter Company Bank Account Balance : 50000

Employee Name 4 - Employee  Pin 4 Salary Paid and Successfully update In Database.
Employee Name 5 - Employee  Pin 5 Salary Paid and Successfully update In Database.
Employee Name 6 - Employee  Pin 6 Salary Paid and Successfully update In Database.
Employee Name 7 - Employee  Pin 7 Salary Paid and Successfully update In Database.
Employee Name 8 - Employee  Pin 8 Salary Paid and Successfully update In Database.
Employee Name 9 - Employee  Pin 9 Salary Paid and Successfully update In Database.
Employee Name 10 - Employee  Pin 10 Salary Paid and Successfully update In Database.

The Money Has Been Deposited In The Company Account 2 Times
First Deposit Amount : 10000.0
Last Deposit Amount : 50000.0
```
